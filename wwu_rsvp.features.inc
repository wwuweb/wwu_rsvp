<?php
/**
 * @file
 * wwu_rsvp.features.inc
 */

/**
 * Implements hook_views_api().
 */
function wwu_rsvp_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
