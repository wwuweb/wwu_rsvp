<?php
/**
 * @file
 * wwu_rsvp.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function wwu_rsvp_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-event-field_seats_remaining'
  $field_instances['node-event-field_seats_remaining'] = array(
    'bundle' => 'event',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'number',
        'settings' => array(
          'decimal_separator' => '.',
          'prefix_suffix' => TRUE,
          'scale' => 0,
          'thousand_separator' => ' ',
        ),
        'type' => 'number_integer',
        'weight' => 12,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'fences_wrapper' => 'div',
    'field_name' => 'field_seats_remaining',
    'label' => 'Remaining Seats',
    'required' => 0,
    'settings' => array(
      'max' => '',
      'min' => '',
      'prefix' => '',
      'suffix' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'number',
      'settings' => array(),
      'type' => 'number',
      'weight' => 13,
    ),
  );

  // Exported field_instance: 'node-event-field_rsvp_link'
  $field_instances['node-event-field_rsvp_link'] = array(
    'bundle' => 'event',
    'default_value' => array(
      0 => array(
        'vname' => 'event_rsvp_link_field|default',
        'vargs' => '[node:nid]',
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'viewfield',
        'settings' => array(),
        'type' => 'viewfield_default',
        'weight' => 15,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'fences_wrapper' => 'div',
    'field_name' => 'field_rsvp_link',
    'label' => 'RSVP Link',
    'required' => 0,
    'settings' => array(
      'allowed_views' => array(
        'admin_views_file' => 0,
        'admin_views_node' => 0,
        'admin_views_user' => 0,
        'art_application_review' => 0,
        'basic_directory' => 0,
        'bfa_tumblr_feed_view' => 0,
        'debug_feeds_event_importer' => 0,
        'department_directory' => 0,
        'event_feed' => 0,
        'event_rsvp_link_field' => 'event_rsvp_link_field',
        'events' => 0,
        'events_administration' => 0,
        'events_calendar_calendar_' => 0,
        'facilities' => 0,
        'featured' => 0,
        'feeds_log' => 0,
        'front_page_news' => 0,
        'front_page_news_articles_' => 0,
        'galleries' => 0,
        'mc_weeder' => 0,
        'media_browser_plus' => 0,
        'media_browser_plus_folders' => 0,
        'media_default' => 0,
        'music_area_filters' => 0,
        'og_all_user_group_content' => 0,
        'og_members' => 0,
        'og_members_admin' => 0,
        'og_nodes' => 0,
        'og_user_groups' => 0,
        'productions' => 0,
        'promoted_to_front_page' => 0,
        'special_events' => 0,
        'suggest_edit' => 0,
        'theatre_dance_past_productions' => 0,
        'tweets' => 0,
        'webform_analysis' => 0,
        'webform_results' => 0,
        'webform_submissions' => 0,
        'webform_webforms' => 0,
        'workbench_current_user' => 0,
        'workbench_edited' => 0,
        'workbench_files' => 0,
        'workbench_moderation' => 0,
        'workbench_recent_content' => 0,
      ),
      'force_default' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'viewfield',
      'settings' => array(),
      'type' => 'viewfield_select',
      'weight' => 17,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('RSVP Link');
  t('Remaining Seats');

  return $field_instances;
}
